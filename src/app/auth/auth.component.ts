import { stringify } from '@angular/compiler/src/util';
import { Component, OnInit } from '@angular/core';
import { AuthService} from './auth.service';
@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  providers: [AuthService],
  styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnInit {

  values = [];
  error: any;
  selectedValues: string[] = ['val1','val2'];
  constructor(private AuthService: AuthService) { }

  ngOnInit(): void {
    
  }

  getValues(): void{
    this.AuthService.getValues().subscribe(
      data => this.values = data, 
      error => this.values = ["error"]);
  }

}
