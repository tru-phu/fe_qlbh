import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';


import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { HttpErrorHandler, HandleError } from '../http-error-handler.service';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    Authorization: 'my-auth-token'
  })
};

@Injectable()
export class AuthService {
  baseUrl = 'https://localhost:44320/api/values'; //URL to web API
  private handleError: HandleError;

  constructor(
    private http: HttpClient,
    httpErrorHandler: HttpErrorHandler) {
    this.handleError = httpErrorHandler.createHandleError('AuthService');
  }

  /** GET values from the server */
  getValues(): Observable<string[]> {
    return this.http.get<string[]>(this.baseUrl)
      .pipe(
        catchError(this.handleError('getValues', []))
      );
  }

  //////// Save methods //////////

  /** POST: add a new hero to the database */
  // addHero(Add: string): Observable<string> {
  //   return this.http.post<string>(this.baseUrl, Add, httpOptions)
  //     .pipe(
  //       catchError(this.handleError('Post', Add))
  //     );
  // }
}
