import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthComponent} from './auth/auth.component';
import { CMSComponent} from './cms/cms.component';
const routes: Routes = [
  {path: '', component: AuthComponent},
  {path: 'Auth', component: AuthComponent},
  {path: 'cms',component: CMSComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
