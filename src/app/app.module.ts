import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule} from '@angular/common/http';
import { CheckboxModule } from 'primeng/checkbox';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthComponent } from './auth/auth.component';
import { LoginComponent } from  './login/login.component';
import { CMSComponent } from './cms/cms.component';

import { HttpErrorHandler } from './http-error-handler.service';
import { MessageService } from './message.service';
const appRoutes : Routes = [
  { path: '', component: LoginComponent},
  { path: 'Login', component: LoginComponent},
  { path: 'Auth', component: AuthComponent},
  { path: 'CMS', component: CMSComponent},
];

@NgModule({
  declarations: [
    AppComponent,
    CMSComponent,
    AuthComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    CheckboxModule,
    RouterModule.forRoot(
      appRoutes,
        { enableTracing: true, relativeLinkResolution: 'legacy' }
    )
  ],
  providers: [
    HttpErrorHandler, 
    MessageService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
